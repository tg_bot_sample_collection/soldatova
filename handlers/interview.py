from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

available_family_status = ["Замужем/Женат", "Не замужем/Холост", "Разведена/Разведен"]
available_sex = ["Женский", "Мужской", "Другое"]

class InterviewStatus(StatesGroup):
    waiting_for_fio = State()
    waiting_for_age = State()
    waiting_for_sex = State()
    waiting_for_family_status = State()
    waiting_for_about = State()
    waiting_for_renew = State()


async def interview_start(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("<b>Здравствуйте!</b> Я помогу вам сформировать краткую анкету для сайтов знакомств. \n\nПросто"
                         + " ответьте на несколько моих вопрсов, а я подготовлю анкету для Вас!")
    await message.answer("Как вас зовут?")
    await InterviewStatus.waiting_for_fio.set()


async def fio_chosen(message: types.Message, state: FSMContext):
    await message.reply("У вас красивое имя!")
    await state.update_data(user_fio=message.text)
    await InterviewStatus.next()
    await message.answer("Сколько вам полных лет?")


async def age_chosen(message: types.Message, state: FSMContext):
    try:
        age = int(message.text.lower())
    except ValueError:
        await message.answer("Вы ввели нецелое число")
        return
    if age < 1 or age > 99:
        await message.answer("Не могу поверить, что вам столько лет! Скажете правду?")
        return
    await state.update_data(age=age)
    await InterviewStatus.next()
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    for sex in available_sex:
        keyboard.add(sex)
    await message.answer("Какой у вас пол?", reply_markup=keyboard)


async def sex_chosen(message: types.Message, state: FSMContext):
    if message.text not in available_sex:
        await message.answer("Пожалуйста, выберите пол из списка.")
        return
    await state.update_data(sex=message.text)

    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    for fs in available_family_status:
        keyboard.add(fs)
    # Для последовательных шагов можно не указывать название состояния, обходясь next()
    await InterviewStatus.next()
    await message.answer("Укажите семейное положение.", reply_markup=keyboard)


async def fs_chosen(message: types.Message, state: FSMContext):
    if message.text not in available_family_status:
        await message.answer("Пожалуйста, выберите статус из списка.")
        return
    await state.update_data(family_status=message.text)
    await InterviewStatus.next()
    await message.answer("Расскажите немного о себе (2-3 предложения)")


async def about_chosen(message: types.Message, state: FSMContext):
    await state.update_data(about=message.text)
    await message.answer("Отлично! Вот ваша анкета:")
    await message.answer(await get_interview_text(state))
    await InterviewStatus.next()


async def ready_to_next(message: types.Message, state: FSMContext):
    await message.answer("Хотите начать сначала? Используйте /start")


async def interview_reset(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Ваша анкета удалена")


async def interview_get(message: types.Message, state: FSMContext):
    message.answer("Сейчас ваша анкета выглядит так:")
    await message.answer(await get_interview_text(state))


async def get_interview_text(state: FSMContext):
    user_data = await state.get_data()
    fio = user_data.get('user_fio') or "Не заполнено"
    age = user_data.get('age') or "Не заполнено"
    sex = user_data.get('sex') or "Не заполнено"
    fs = user_data.get('family_status') or "Не заполнено"
    about = user_data.get('about') or "Не заполнено"
    res_string = "<b>Ваше имя</b>: " + fio + "\n" + "<b>Возраст</b>: " + str(age) + '\n' \
                 + "<b>Пол</b>: " + sex + '\n' \
                 + "<b>Семейное положение</b>: " + fs + '\n' \
                 + "<b>О себе</b>: " + about
    return res_string


def register_handlers_interview(dp: Dispatcher):
    dp.register_message_handler(interview_start, commands="start", state="*")
    dp.register_message_handler(interview_reset, commands="cancel", state="*")
    dp.register_message_handler(interview_get, commands="get", state="*")
    dp.register_message_handler(fio_chosen, state=InterviewStatus.waiting_for_fio)
    dp.register_message_handler(age_chosen, state=InterviewStatus.waiting_for_age)
    dp.register_message_handler(sex_chosen, state=InterviewStatus.waiting_for_sex)
    dp.register_message_handler(fs_chosen, state=InterviewStatus.waiting_for_family_status)
    dp.register_message_handler(about_chosen, state=InterviewStatus.waiting_for_about)
    dp.register_message_handler(ready_to_next, state=InterviewStatus.waiting_for_renew)
