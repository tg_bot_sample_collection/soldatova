import logging
from aiogram import Bot, Dispatcher, executor, types, asyncio
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import BotCommand
from aiogram.utils.exceptions import BotBlocked
import aiogram.utils.markdown as fmt
import configparser


from handlers.parser import register_handlers_interview


logger = logging.getLogger(__name__)

# Регистрация команд, отображаемых в интерфейсе Telegram
async def set_commands(bot: Bot):
    commands = [
        BotCommand(command="/start", description="Начать"),
        BotCommand(command="/get", description="Получить информацию о себе"),
        BotCommand(command="/cancel", description="Удалить данные")
    ]
    await bot.set_my_commands(commands)


async def main():
    # Настройка логирования в stdout
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )
    logger.error("Starting bot")

    # Парсинг файла конфигурации
    config = configparser.ConfigParser()
    config.read('config/bot.ini')
    # Объявление и инициализация объектов бота и диспетчера
    bot = Bot(token=config['BOT_PROPERTIES']['BOT_KEY'], parse_mode=types.ParseMode.HTML)
    dp = Dispatcher(bot, storage=MemoryStorage())
    # Регистрация хэндлеров
    register_handlers_interview(dp)
    @dp.message_handler()
    async def any_text_message(message: types.Message):
        await message.answer("Начните с команды /start")




    # Установка команд бота
    await set_commands(bot)

    # Запуск поллинга
    # await dp.skip_updates()  # пропуск накопившихся апдейтов (необязательно)
    await dp.start_polling()

if __name__ == '__main__':
    asyncio.run(main())


